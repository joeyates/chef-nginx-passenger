name             'nginx-passenger'
maintainer       'Joe Yates'
maintainer_email 'joe.g.yates@gmail.com'
license          'All rights reserved'
description      'Installs/Configures nginx-passenger'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'rbenv'

