#
# Cookbook Name:: nginx-passenger
# Recipe:: default
#
# Copyright 2013, Joe Yates
#
# License: MIT
#
# This cookbook is based in part on the following gist:
# https://gist.github.com/kiesia/3051708

# Remove existing package-based installation of nginx

%w{nginx nginx-light nginx-full nginx-extras}.each do |pkg|
  package pkg do
    action      :purge
  end
end

nginx_var_log_path = File.join('', 'var', 'log', 'nginx')

directory nginx_var_log_path do
  action      :delete
  recursive   true
  not_if      { File.symlink?(nginx_var_log_path) }
end

%w{build-essential libcurl4-openssl-dev libpcre3-dev libssl-dev zlib1g-dev}.each do |pkg|
  package     pkg
end

rbenv_gem 'passenger' do
  version     node['nginx']['passenger']['version']
end

rbenv_rehash 'rehash!'

nginx_version          = "nginx-#{node['nginx']['version']}"
tarball_name           = "#{nginx_version}.tar.gz"
source_path            = File.join('', 'usr', 'src')
nginx_tarball_pathname = File.join(source_path, tarball_name)
nginx_source_path      = File.join(source_path, nginx_version)
nginx_executable       = File.join(node['nginx']['path'], 'sbin', 'nginx')

remote_file 'download nginx' do
  action      :create_if_missing
  owner       'root'
  group       'root'
  mode        '0644'
  path        nginx_tarball_pathname
  source      "http://nginx.org/download/#{tarball_name}"
  not_if      { File.directory?(nginx_tarball_pathname) or File.exists?(nginx_executable) }
end

execute 'extract nginx' do
  command     "tar xf #{tarball_name}"
  cwd         source_path
  not_if      { File.directory?(nginx_source_path) or File.exists?(nginx_executable) }
end

rbenv_script 'build nginx' do
  root_path   node['rbenv']['root_path']
  code        <<-EOT
passenger-install-nginx-module \
  --auto \
  --prefix='#{node['nginx']['path']}' \
  --nginx-source-dir='#{nginx_source_path}' \
  --extra-configure-flags='--with-ipv6 --with-http_realip_module'
  EOT

  not_if      { File.exists?(nginx_executable) }
end

nginx_executauble_symlink = File.join('', 'usr', 'local', 'sbin', 'nginx')

link nginx_executauble_symlink do
  to          nginx_executable
end

link nginx_var_log_path do
  to          File.join(node['nginx']['path'], 'logs')
end

directory File.join(node['nginx']['path'], 'sites-available') do
  mode        '0755'
end

directory File.join(node['nginx']['path'], 'sites-enabled') do
  mode        '0755'
end

# Configuration file

ruby_patch_level   = node['rbenv']['global']
ruby_version       =
  case ruby_patch_level
  when /^1\.9/
    '1.9.1'
  when /^2/
    '2.0.0'
  else
    raise 'Unknown ruby version'
  end
rbenv_version_path = File.join(node['rbenv']['root_path'], 'versions', ruby_patch_level)
rbenv_gems_path    = File.join(rbenv_version_path, 'lib', 'ruby', 'gems', ruby_version, 'gems')
passenger_root     = File.join(rbenv_gems_path, "passenger-#{node['nginx']['passenger']['version']}")
passenger_ruby     = File.join(rbenv_version_path, 'bin', 'ruby')

ruby_block 'check passenger path' do
  block do
    if not File.directory?(passenger_root)
      Chef::Application.fatal!("The expected passenger root directory '#{passenger_root}' is missing")
    end
  end
  action      :nothing
end

ruby_block 'check ruby path' do
  block do
    if not File.exist?(passenger_ruby)
      Chef::Application.fatal!("The expected ruby executable '#{passenger_ruby}' is missing")
    end
  end
  action      :nothing
end

nginx_conf = File.join(node['nginx']['path'], 'conf', 'nginx.conf')

template nginx_conf do
  owner       'root'
  group       'root'
  mode        '0644'
  source      'nginx.conf.erb'
  variables   :passenger_root => passenger_root,
              :passenger_ruby => passenger_ruby
  notifies    :reload, 'service[nginx]'
end

template File.join(node['nginx']['path'], 'conf', 'mime.types') do
  owner       'root'
  group       'root'
  mode        '0644'
  source      'nginx.mime.types.erb'
  notifies    :reload, 'service[nginx]'
end

# Init script

nginx_init_script = File.join('', 'etc', 'init.d', 'nginx')

template nginx_init_script do
  owner       'root'
  group       'root'
  mode        '0755'
  source      'nginx.init.erb'
  variables   :src_binary => nginx_executauble_symlink
end

bash 'set init links' do
  user        'root'
  code        'update-rc.d -f nginx defaults'
end

service 'nginx' do
  supports    :status => true,
              :restart => true,
              :reload => true
  action      [:enable, :start]
end

